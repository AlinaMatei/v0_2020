import React, { Component } from 'react'
import CompanyStore from '../stores/CompanyStore'
import Company from './Company'

class CompanyList extends Component {
	constructor(){
		super()
		this.state = {
			companies : []
		}
	}
	deleteCompany = (id) => {
			this.store.deleteOne(id)
	}
			
	componentDidMount(){
		this.store = new CompanyStore()
		this.setState({
			companies : this.store.getAll()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				companies : this.store.getAll()
			})			
		})
	}
	
  render() {
    return (
      <div>{
		     this.state.companies.map((comp,index) =>{
		  		return <Company item={comp}  key={index} onDelete={this.deleteCompany}/>
		  		 })
		     }
      </div>
    )
  }
}

export default CompanyList
