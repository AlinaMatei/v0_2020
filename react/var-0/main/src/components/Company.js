import React, { Component } from 'react'

class Company extends Component{
    
    deleteCompany = () =>{
        this.props.onDelete(this.props.item.id)
    }
    
    render(){
        return(
        <div>
            <input type="button" value="delete" onClick={this.deleteCompany}/>
        </div>    
        )
    }
}

export default Company
