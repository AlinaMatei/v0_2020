function bowdlerize(input, dictionary){
    if(typeof input !== "string"){
        throw new Error("Input should be a string");
    }
    
    dictionary.forEach(w =>{
        if(typeof w !== "string"){
            throw new Error("Invalid dictionary format");
        }
    })
    
    dictionary.forEach(w =>{
        input.split(" ").forEach(i =>{
            if(w === i.toLowerCase()){
                input =input.replace(i, i[0]+"*".repeat(i.length-2)+i[i.length-1]);
                return input;
            }
        })
    })
    
    
    return input;
}

const app = {
    bowdlerize
};

module.exports = app;